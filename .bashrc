#
# ~/.bashrc
#

# If not running interactively, don't do anything

export PATH="$PATH:/home/mcnuggetsx20/.local/bin"
export PATH="$PATH:/home/mcnuggetsx20/Desktop"

GREEN="\[$(tput setaf 2)\]"
YELLOW="\[$(tput setaf 3)\]"
BLUE="\[$(tput setaf 4)\]"
ORANGE="\[$(tput setaf 9)\]"
VIOLET="\[$(tput setaf 5)\]"
DARK_VIOLET="\[$(tput setaf 13)\]"
RED="\e[31m"
RESET="\[$(tput sgr0)\]"
[[ $- != *i* ]] && return

COLUMNS=$(tput cols)

#the first one is the default, the second one shows the whole directory
#ZEBY BYL TYLKO TEN JEDEN FOLDER TO MUSI BYC W (DUZE)

PS1="[${VIOLET}\e[1m\u${RESET} \w]: ${BLUE}\t${RESET}\n${GREEN} >$ ${RESET}"
#PS1="[\u \w]`printf "%${COLUMNS}s" "\t" `$PS1"
#PS1="[\w]${GREEN}$ ${RESET}"
#PS1='[\u@\h \w]$ '

#aliasy
alias qconf='internal_nvim ~/.config/qtile/config.py'
alias l='exa --group-directories-first --icons -lagBh'
alias pacinstall='sudo pacman -S'
alias pacclear='echo y | sudo pacman -Scc; sudo pacman -Scc --noconfirm'
alias pacremove='sudo pacman -Rns'
alias pacrefresh='sudo pacman -Syy'
alias pacupgrade='configpush; sudo pacman -Syyu --noconfirm; sudo pacman -Scc --noconfirm; echo y | sudo pacman -Scc; echo; speed'
alias pcom='internal_nvim ~/.config/picom/picom.conf &'
alias pb='python -B'
alias lf='lfub'
alias cd='nvim_autocd'
alias vim='internal_nvim'
#alias setCover='eyeD3 --add-image $2:FRONT_COVER:front $1' #SONG / IMAGE
alias take='_take'
#alias picom='/mnt/hdd/Program-Files/picom-pij/build/src/start'
alias mk='gcc -o main main.c -lX11 -Wextra -Wall'
alias mkmips='java -jar ~/Documents/program-files/mars-mips/Mars4_5.jar'
alias fetch='fastfetch'
alias speed='xset r rate 200 90'
alias tv='xrandr --output HDMI-2 --mode 1920x1080 --rate 60 --above eDP-1'
##########################

bind "set completion-ignore-case on"

function chase() {
    sudo pacman -Rns $(pacman -Qq | grep $1)
}

function gitpush(){
    DATE=$(date "+%d %b %Y %H:%M:%S")
    git add -A
    git commit -m "${DATE}"
    git push
}

nvim_autocd(){
    builtin cd "$@"
    if [ -v NVIM ]; then
        (nvim_client_python -cd &) > /dev/null
    fi
}

internal_nvim(){
    if [ -v NVIM ]; then
        (nvim_client_python -c "tabnew $1" &) > /dev/null
    fi
}

_take(){
    mkdir "$@"
    cd "$@"
}

setBat0Start(){
    sudo echo $1 | sudo tee /sys/class/power_supply/BAT0/charge_start_threshold > /dev/null
    echo Set charge_start for BAT0 to $1
}
setBat0Stop(){
    sudo echo $1 | sudo tee /sys/class/power_supply/BAT0/charge_stop_threshold > /dev/null
    echo Set charge_stop for BAT0 to $1
}
setBat1Start(){
    sudo echo $1 | sudo tee /sys/class/power_supply/BAT1/charge_start_threshold > /dev/null 
    echo Set charge_start for BAT1 to $1
}
setBat1Stop(){
    sudo echo $1 | sudo tee /sys/class/power_supply/BAT1/charge_stop_threshold > /dev/null
    echo Set charge_stop for BAT1 to $1
}
