from libqtile.layout.floating import Floating
from libqtile.layout.xmonad import MonadThreeCol
from libqtile.layout.max import Max
from libqtile.config import Match

floatLayout = Floating(
    border_width=0,
)

mainLayout= MonadThreeCol(
        margin = [20, 20, 20, 20],
        border_width = 2,
        ratio = 0.5,
        change_ratio=0.025,
        single_margin = [30, 30, 20, 30],
        single_border_width = 0,
        border_focus = '#8a2387',
        new_client_position='before_current',
    )

maxLayout = Max()
