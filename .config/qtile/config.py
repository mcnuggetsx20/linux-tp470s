import subprocess

from libqtile import bar, layout, qtile, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy

from lib import *
import user_widgets
import user_layouts

keys = [

    #app launching
    Key([mod], 'p', lazy.spawn(dmenu)),
    Key([mod], 'o', lazy.spawn(dmenu_apps)),
    Key([sup], 'b', lazy.spawn(browser)),
    Key([sup], "f", lazy.spawn(file_browser), desc="Launch terminal"),
    Key([sup], "v", lazy.spawn(sound_control), desc="Launch terminal"),
    Key([sup], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    #screen backlight
    Key([], 'XF86MonBrightnessDown', lazy.spawn(f'{bin_dir}/ibacklight -300')),
    Key([], 'XF86MonBrightnessUp', lazy.spawn(f'{bin_dir}/ibacklight 300')),

    #screenshots
    Key([mod, 'shift'], 's', lazy.spawn(f'{bin_dir}/screenshot -s')),

    #audio
    Key([],    'XF86AudioRaiseVolume', lazy.spawn('pamixer -i 5')),
    Key([],    'XF86AudioLowerVolume', lazy.spawn('pamixer -d 5')),


    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    
    #Manipulate windows
    Key([mod], 'i', lazy.layout.grow()),
    Key([mod], 'm', lazy.layout.shrink()),
    Key([mod], "n", lazy.layout.reset(), desc="Reset all window sizes"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([sup], "BackSpace", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen on the focused window",),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating on the focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod], 'e', lazy.to_screen(1)),
    Key([mod], 'w', lazy.to_screen(0)),
]


groups = [Group(name=str(i), position=i, layouts = [user_layouts.mainLayout, user_layouts.maxLayout]) for i in range(1, 5)]
groups.append(
    Group(
        name = '5',
        position = 5,
        layouts = [user_layouts.floatLayout],
        matches =[
            Match(wm_class='signal'),
        ]
    )
)

for i in groups:
    keys.extend(
        [
            # mod + group number = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod + shift + group number = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=False),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod + shift + group number = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )


widget_defaults = dict(
    font="sans",
    fontsize=16,
    padding=3,
    foreground='#bbbbbb',
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        wallpaper='~/Pictures/wallpapers/night_sand.jpeg',
        wallpaper_mode='fill',
        bottom=bar.Bar(
            margin = [0, 1090, 5, 10],
            background='060708',
            widgets=[
                user_widgets.mGroupBox,
                user_widgets.mCurrentLayout,
                user_widgets.mSpacer,
                user_widgets.mWindowName,
                user_widgets.mSystray,
                user_widgets.mBatteryPerc,
                user_widgets.mBatteryTime,
                user_widgets.mClock,
            ],
            size = 30,
            border_width=[2, 2, 2, 2],  # Draw top and bottom borders
            border_color=["748b97"] * 4  # Borders are magenta
        ),
        # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
        # By default we handle these events delayed to already improve performance, however your system might still be struggling
        # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
        # x11_drag_polling_rate = 60,
    ),
    Screen(
        wallpaper='~/Pictures/wallpapers/sandworm.jpg',
        wallpaper_mode='fill',
        top=bar.Bar(
            margin = [5, 0, 0, 0],
            widgets=[
                user_widgets.mCurrentLayout,
                user_widgets.mGroupBox,
                user_widgets.mWindowName,
                user_widgets.mSystray,
                user_widgets.mBatteryPerc,
                user_widgets.mBatteryTime,
                user_widgets.mClock,
            ],
            size = 24,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
        # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
        # By default we handle these events delayed to already improve performance, however your system might still be struggling
        # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
        # x11_drag_polling_rate = 60,
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

auto_minimize = False

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# xcursor theme (string or None) and size (integer) for Wayland backend
wl_xcursor_theme = None
wl_xcursor_size = 24
wmname = "LG3D"
