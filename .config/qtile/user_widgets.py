from libqtile import bar, widget
import subprocess


mCurrentLayout = widget.CurrentLayout(fmt='  {}')

mGroupBox = widget.GroupBox()

mWindowName = widget.WindowName(max_chars=15)

mSystray = widget.Systray()

mBatteryPerc = widget.GenPollText(
    func=
    lambda: ' / '.join(subprocess.check_output(['battery'], encoding='utf-8')[:-1].split()),
    update_interval=3,
)

mBatteryTime = widget.Battery(
    format = '{hour:d}:{min:02d}'
)

mClock = widget.Clock(
    format="|| %H:%M:%S || %d.%m.'%y",
)

mSpacer = widget.Spacer(12)
