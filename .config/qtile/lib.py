sup =               'mod4'
mod =               'mod1'
terminal =          "alacritty -e nvim --cmd term -c 'set ma' -c startinsert"

dmenu_args =        '-x 960 -y 10 -z 900 -i'
dmenu =             f'dmenu_run {dmenu_args}'
dmenu_apps =        f'j4-dmenu-desktop --dmenu \"dmenu {dmenu_args}\"'

browser =           'brave --password-store=basic'
bin_dir =           '/home/mcnuggetsx20/bin'
file_browser =      'pcmanfm'
sound_control =     'pavucontrol'
task_manager =      'gnome-system-monitor'
