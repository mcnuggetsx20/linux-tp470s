#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc
export PYTHONPATH="${PYTHONPATH}:/home/mcnuggetsx20/.config/qtile"
export PATH="$PATH:/home/mcnuggetsx20/bin"
export GIT_CONFIG_DIR=/home/mcnuggetsx20/Documents/repos/linux-tp470s


# BEGIN opam configuration
# This is useful if you're using opam as it adds:
#   - the correct directories to the PATH
#   - auto-completion for the opam binary
# This section can be safely removed at any time if needed.
test -r '/home/mcnuggetsx20/.opam/opam-init/init.sh' && . '/home/mcnuggetsx20/.opam/opam-init/init.sh' > /dev/null 2> /dev/null || true
# END opam configuration

export DMENU_ARGS='-x 750 -y 0 -z 450 -i'
export DMENU="dmenu_run ${DMENU_ARGS}"
export DMENU_APPS="j4-dmenu-desktop -d \"dmenu $DMENU_ARGS\""
